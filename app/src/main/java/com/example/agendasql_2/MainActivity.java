package com.example.agendasql_2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.AgendaDbHelper;
import database.Contacto;
import database.AgendaContactos;

public class MainActivity extends AppCompatActivity {
    //private Button btnPrueba;
    //private TextView lblPrueba;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnListar;
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private AgendaContactos db;
    private Contacto savedContact;
    private long id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //btnPrueba = findViewById(R.id.btnPrueba);
        //lblPrueba = findViewById(R.id.lblPrueba);

        edtNombre= (EditText) findViewById(R.id.txtNombre);
        edtTelefono= (EditText) findViewById(R.id.txtTel1);
        edtTelefono2= (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas =(EditText) findViewById(R.id.txtNota);
        cbxFavorito = (CheckBox)findViewById(R.id.chkFavorito);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnListar = (Button) findViewById(R.id.btnListar);
        db = new AgendaContactos(MainActivity.this);
        /*btnPrueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dataBaseName = "";
                AgendaDbHelper agendaDbHelper = new AgendaDbHelper(MainActivity.this);
                SQLiteDatabase db;
                db = agendaDbHelper.getWritableDatabase();
                dataBaseName = agendaDbHelper.getDatabaseName();
                lblPrueba.setText(dataBaseName);
            }
        });*/
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validacion de los campos
                if(edtNombre.getText().toString().equals("")||
                        edtDireccion.getText().toString().equals("")||
                        edtTelefono.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgerror,
                            Toast.LENGTH_SHORT).show();
                }else{

                    Contacto nContacto = new Contacto();
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    if(cbxFavorito.isChecked()){
                        nContacto.setFavorito(1);
                    }else{
                        nContacto.setFavorito(0);
                    }

                    db.openDataBase();

                    if(savedContact== null){
                        long idx= db.insertarContacto(nContacto);
                        Toast.makeText(MainActivity.this, "Se agregó contacto con ID: " + idx,Toast.LENGTH_SHORT).show();
                    }else{
                        db.UpdateContacto(nContacto, id);
                        Toast.makeText(MainActivity.this, "Se actualizó el registro: "+ id,Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar(); // codificacion del boton limpiar
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ListActivity.class);
                startActivityForResult(intent,0);
            }
        });
    }
    public void limpiar()
    {
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (Activity.RESULT_OK == resultCode) {
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDomicilio());
            edtNotas.setText(contacto.getNotas());
            if (contacto.getFavorito() > 0) {
                cbxFavorito.setChecked(true);
            }
        } else {
            limpiar();
        }

    }
}
