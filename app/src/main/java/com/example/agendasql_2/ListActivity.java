package com.example.agendasql_2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import database.AgendaContactos;
import database.Contacto;

public class ListActivity extends android.app.ListActivity {
    private AgendaContactos agendaContactos; //agregado
    private Button btnNuevo; //agregado
    private ArrayList<Contacto> listacontacto; //agregado
    private MyArrayAdapter adapter; // agregado

    //llenamos la lista
    public void llenarlista(){
        agendaContactos.openDataBase();
        listacontacto= agendaContactos.allContactos();
        agendaContactos.cerrar();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        agendaContactos = new AgendaContactos(this);

        llenarlista();
        adapter = new MyArrayAdapter(this, R.layout.layout_contacto,listacontacto);
        String str =adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    //clase anidada dentro del array
    class MyArrayAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int textViewResourceId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int resource, ArrayList<Contacto> contactos) {
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }
        public View getView(final int position, View converView, ViewGroup viewGroup){
            LayoutInflater layoutInflater =(LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view= layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre =(TextView) view.findViewById(R.id.lblNombre);
            TextView lblTelefono =(TextView) view.findViewById(R.id.lbltel1);

            Button btnModificar = (Button)findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }

            lblNombre.setText(contactos.get(position).getNombre());
            lblNombre.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    agendaContactos.openDataBase();
                    agendaContactos.deleteContacto(contactos.get(position).get_ID());
                    agendaContactos.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle(); //Se agrega el objeto intent para enviar la info al main
                    bundle.putSerializable("contacto",contactos.get(position)); //
                    Intent i = new Intent( ListActivity.this, MainActivity.class); //
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });

            return view;
        }
    }

}
